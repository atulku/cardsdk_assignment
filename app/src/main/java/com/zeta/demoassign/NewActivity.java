package com.zeta.demoassign;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zeta.demoassign.databinding.ActivityNewBinding;
import com.zeta.demoassign.utils.MyApplication;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.zeta.android.commons.kvstore.BasicKVStore;
import in.zeta.apollo.cards.CardsService;
import in.zeta.apollo.encryptedstore.EncryptedStore;
import in.zeta.apollo.encryptedstore.secure_store.SecureStore;
import timber.log.Timber;

public class NewActivity extends AppCompatActivity {


    // assignment used credentials
/*    String tenantAuthToken = "eyJhbGciOiJFUzI1NiJ9.eyJ0ZW5hbnRVbmlxdWVWZWN0b3JJZCI6ImExODY0YTkzLWM0ODQtNGE4Zi05MGI1LWYzNzQxMjM2N2ZlMyIsImlhdCI6MTYzMzA2OTc0MiwiZXhwIjoxNjM4MzQ3Njk4fQ.AHuXsp6hMxBcJXdkAaLr7kBXIHxOlr_uJyzl33FyS0vbQlrTd_acAcnKunEuk6xdDmNSdPD8hh2ECN0jpz6jWA";
    String accountHolderId = "a1864a93-c484-4a8f-90b5-f37412367fe3";  // static of some user
    String cardId = "cff73dfb-64e4-4968-8b89-f338f5f56330";// card id*/

    String tenantAuthToken = "eyJhbGciOiJFUzI1NiJ9.eyJ0ZW5hbnRVbmlxdWVWZWN0b3JJZCI6IjgyOHUyM1cxdkdfelo5M2pFTmZsMXc9PSIsImlhdCI6MTYzOTEyNjM5NiwiZXhwIjoxNjM5MTI2OTk2fQ.TPVJ636cKkYgDRPRBI78-cXJNjrkVJ9QZfBCkrMAsxgZ8Z59IF6yAfFdiXlDBlbgHlNcZCQmrK08Uk8aaI_NNQ";
//    String accountHolderId = "828u23W1vG_zZ93jENfl1w==";  // mobile relam static of some user
    String accountHolderId = "Ii42jmtQtqJ6bVj1HJagvg==";  // wallet relamstatic of some user

    String cardId = "f21164dc-be1e-4f0c-8151-594abdd5d291";// card id


    String resourceID = "10abe380-c28c-425f-aa5a-760410fd4d14";

    private ActivityNewBinding activityNewBinding;
    final Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewBinding = ActivityNewBinding.inflate(getLayoutInflater());
        setContentView(activityNewBinding.getRoot());
//        setContentView(R.layout.activity_new);

        // use the below code after the user has logged in to the app afyer login in LOginAcitivty
        authenticateSDK();                                                  ///////1st step

        // for security and protection this is included for pin or app lock
        inilializeSDK();                                                    ///////2nd  step

        listners();                                                    ///////3rd step to show card

    }

    private void listners() {

        activityNewBinding.btnChkSDKStatus.setOnClickListener(view -> {
            boolean isSDKSetupDone = CardsService.getInstance().checkSetupSDKDone();
            runOnUiThread(() -> {
                if (isSDKSetupDone) {
                    Toast.makeText(getApplicationContext(), "--> SDK Setup is successful", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "--> SDK Setup is not yet done", Toast.LENGTH_LONG).show();
                }
            });
        });

        activityNewBinding.btnShowCard.setOnClickListener(view -> {
            // setup the card view...
            setupViewOfCards();
        });

        activityNewBinding.btnGetSuperPin.setOnClickListener(view -> {
            CardsService.getInstance().getSuperPinForResourceUI(this, resourceID, (superPin, exp) -> {
                if (exp != null) {
                    // Exception in getting the super pin
                    this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Super exp: " + exp.toString() + exp.getMessage(), Toast.LENGTH_LONG).show());
                } else {
                    // Super pin fetched successfully
                    this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Super pin fetched success.", Toast.LENGTH_LONG).show());
                }
            });
        });

        activityNewBinding.btnForceAuthenticate.setOnClickListener(view -> {
            // this api is used to take PIN authentication from the user forecefully, BTW, in addtion to that SDK also
            // take pin autorization automatically whenerver required, but if you want forefully this is the way.
            forceAuthenticateApi();
        });

        activityNewBinding.btnLogoutUser.setOnClickListener(view -> {
            CardsService.getInstance().logout(() -> {
                // The user is logged out …
                this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> User is logged out ", Toast.LENGTH_LONG).show());
            });
        });

        activityNewBinding.btnCardPanData.setOnClickListener(view -> {
//            getCardPanData();
            getCardData();
        });

    }

    void getCardData() {
        CardsService.getInstance().getCardData(cardId, this, (data, exp) -> {
            if (exp != null) {
                // Exception in getting the super pin
                this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Super exp: " + exp.toString() + exp.getMessage(), Toast.LENGTH_LONG).show());
            } else {
                Timber.d("The response is: " + gson.toJson(data));
                // Super pin fetched successfully
                this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Super pin fetched success.", Toast.LENGTH_LONG).show());
            }
        });
    }


/*
    public  void addCardEventListner()
    {
        CardsService.CardsEventListener cardsEventListener = (event, exp) -> {
            Timber.d("Event Received from CARDS SDK: " + event);
            //Perform the required functionality here..
            if (exp != null) {
                Timber.d("Error: " + exp.getMessage());
            }
        };

// Use this to add the listener
        CardsService.getInstance().addCardsEventsListener(cardsEventListener);

// Use this to remove the listener
        CardsService.getInstance().removeCardsEventsListener(cardsEventListener);
    }
*/


    private void getCardPanData() {
        //Get Card Pan Data
        Timber.d("Get card by API triggered");
        MyApplication.Companion.getCardsService().getCardData(cardId, this, (cardsPan, exp) -> {
            Timber.d("The response is: " + gson.toJson(cardsPan));
            runOnUiThread(() -> {
                if (exp == null) {
                    this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Get card data by API success: " + cardsPan.toString(), Toast.LENGTH_LONG).show());
                } else {
                    this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "" + exp.getMessage(), Toast.LENGTH_LONG).show());
                }
            });
        });

    }

    private void forceAuthenticateApi() {
        boolean clearSession = true;
        CardsService.getInstance().forceAuthenticate(this, new EncryptedStore.SecuredStoreSuccessListener() {
                    @Override
                    public void onSuccess() {
                        // Force Authentication success.
                        runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Super pin fetched success.", Toast.LENGTH_LONG).show());

                    }

                    @Override
                    public void onFailure(@NotNull Throwable throwable) {
                        // Force Authentication failed.
                        runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Super exp: " + throwable.toString(), Toast.LENGTH_LONG).show());


                    }
                }
                , clearSession);

    }


    private void authenticateSDK() {
        // "tenantAuthToken" is jwtToken here
//        CardsService.getInstance().authenticateSDK(tenantAuthToken, accountHolderId);
        Log.e("resp22_before", "called before authenticate");
        MyApplication.Companion.getCardsService().authenticateSDK(tenantAuthToken, accountHolderId);
        Log.e("resp22_after", "called after authenticate");
    }

    private void inilializeSDK() {
        CardsService.SdkInitListener sdkInitListener = (success, exp) -> {
            // Handle the response here ...
            if (success) {
                this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Passed", Toast.LENGTH_LONG).show());
            } else {
                this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "--> Failed--" + exp.getMessage(), Toast.LENGTH_LONG).show());
            }
        };

        MyApplication.Companion.getCardsService().setupSDK(this, sdkInitListener, null, null);
    }


    private void setupViewOfCards() {


        JsonObject jsonPayload = new JsonObject();
        jsonPayload.addProperty("company_name", "Zeta-India");
        jsonPayload.addProperty("background_image", "https://abc.png");

        CardsService.getInstance().getCardView(this, cardId, "card_EN_UIVIEW", jsonPayload, cardsViewChangeInterceptor, (view, exp) -> {
            if (view != null) {
                // render to view received from cards sdk here..
                Log.d("CARD view_status: ", view.toString());


                activityNewBinding.frameLayout.setVisibility(View.VISIBLE);
                activityNewBinding.frameLayout.removeAllViews();
                activityNewBinding.frameLayout.addView(view, 0);


            } else if (exp != null) {
                Toast.makeText(this, exp.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    CardsService.CardsViewChangeInterceptor cardsViewChangeInterceptor = new CardsService.CardsViewChangeInterceptor() {
        InterceptDone interceptDone = null;

        @Override
        public void onIntercept(@NotNull String requestedState) {
            Log.d("CARD This action was intercepted with requested state: ", requestedState);
/*
   Here, we can write the code we want to perform whenever the state
          change is triggered by the user. Once done, we can call the interceptDone.done() function to continue the regular flow.
*/
            if (interceptDone != null) {
                interceptDone.done();
            }
        }

        @Override
        public void setInterceptDone(@NotNull CardsService.CardsViewChangeInterceptor.InterceptDone interceptDone) {
            this.interceptDone = interceptDone;
        }
    };

}