package com.zeta.demoassign.others

import `in`.zeta.apollo.cards.CardsService
import android.app.Activity
import android.os.Bundle
import `in`.zeta.apollo.cards.CardsService.SdkInitListener
import android.widget.Toast
import `in`.zeta.apollo.cards.CardsService.CardsViewChangeInterceptor.*
import com.zeta.demoassign.R


class MainActivity : Activity() {

    val tenantAuthToken: String =
        "eyJhbGciOiJFUzI1NiJ9.eyJ0ZW5hbnRVbmlxdWVWZWN0b3JJZCI6ImExODY0YTkzLWM0ODQtNGE4Zi05MGI1LWYzNzQxMjM2N2ZlMyIsImlhdCI6MTYzMjk5OTIzOCwiZXhwIjoxNjM4MzQ3Njk4fQ.w_RSd6tX_1XepjlZJsIabW5v_kKjDRDsozsDtK3nAIKz9Rst5fXYIXKDG_b6xHxfbBfsJLrU28Oez8nTs61-eQ"
    var accountHolderId = "a1864a93-c484-4a8f-90b5-f37412367fe3"  // static of some user

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // use the below code after the user has logged in to the app afyer login in LOginAcitivty
        // you can generate TOKEN through curl requst passing PRIVATE KEY
        authenticateSDK()


        // for security and protection this is included for pin or app lock
        invokeApplock()

    }

    private fun invokeApplock() {
        val sdkInitListener =
            object : SdkInitListener {
                override fun done(success: Boolean, exp: Throwable?) {
// Handle the response here ...
                    if (success == true) {
                        runOnUiThread {
                            Toast.makeText(
                                this@MainActivity,
                                "Pin has been set!!!!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                }
            }

        CardsService.getInstance().setupSDK(this, sdkInitListener, null, null)
    }

    private fun authenticateSDK() {
        CardsService.getInstance().authenticateSDK(tenantAuthToken, accountHolderId)
    }
}