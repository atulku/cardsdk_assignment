package com.zeta.demoassign.utils

import `in`.zeta.apollo.cards.CardsService
import `in`.zeta.apollo.cards.CardsServiceBuilder
import android.app.Application
import com.zeta.demoassign.R
import java.util.HashMap

class MyApplication : Application() {

    companion object {
        private const val MANROPE3_REGULAR_FONT_PATH = "fonts/Manrope3-Regular.otf"
        private const val MANROPE3_MEDIUM_FONT_PATH = "fonts/Manrope3-Medium.otf"
        private const val MANROPE3_SEMI_BOLD_FONT_PATH = "fonts/Manrope3-Semibold.otf"
        private const val MANROPE3_BOLD_FONT_PATH = "fonts/Manrope3-Bold.otf"
        private val fontMap: MutableMap<String, String> = HashMap()
        private var cardsService: CardsService? = null

        init {
            fontMap["Manrope3-Regular".lowercase()] =
                MANROPE3_REGULAR_FONT_PATH
            fontMap["Manrope3-Medium".lowercase()] = MANROPE3_MEDIUM_FONT_PATH
            fontMap["Manrope3-Semibold".lowercase()] = MANROPE3_SEMI_BOLD_FONT_PATH
            fontMap["Manrope3-Bold".lowercase()] = MANROPE3_BOLD_FONT_PATH
        }

        fun getCardsService(): CardsService? {
            return cardsService
        }
    }

    override fun onCreate() {
        super.onCreate()
        setupCardsSdk()
    }

    private fun setupCardsSdk() {
        CardsServiceBuilder(this)
            .setLoggingEnabled(true)
            .setSecuredStoreTitle(R.string.title)
            .setSecuredStoreSubtitle(R.string.subtitle)
            .setFontMap(fontMap)
            .useDoor()
            .build()

        // Can get the CardsService instance once it is built.
         cardsService = CardsService.getInstance()
    }




}